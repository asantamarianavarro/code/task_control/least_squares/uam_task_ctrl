#ifndef _TASK_IR_H
#define _TASK_IR_H

// Eigen
#include <eigen3/Eigen/Dense>

namespace UAM {

class CTaskIR
{
  private:

  public:

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskIR();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskIR();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */ 
    static void TaskErrorJac(const int& arm_dof, const double& inf_rad, const Eigen::MatrixXd& d_obs, Eigen::MatrixXd& sigmaIR, Eigen::MatrixXd& JIR, Eigen::MatrixXd& JIR_pseudo);
};

} // End of UAM namespace

#endif
