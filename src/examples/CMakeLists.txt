# create an example application
ADD_EXECUTABLE(uam_task_ctrl_test uam_task_ctrl_test.cpp)

# link necessary libraries
TARGET_LINK_LIBRARIES(uam_task_ctrl_test uam_task_ctrl)
