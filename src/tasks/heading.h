#ifndef _TASK_HEADING_H
#define _TASK_HEADING_H

// Own
#include <common_obj.h>

// Eigen
#include <eigen3/Eigen/Dense>

// Other
#include <math.h>
#include <cmath>

namespace UAM {

class CTaskHEADING
{
  private:

    /**
    * \brief Rotation matrix to euler
    *
    * Returns the euler angles corresponding to the rotation matrix. Specifically returns the angles with the "shortest" path.
    * Based on http://www.staff.city.ac.uk/%7Esbbh653/publications/euler.pdf
    *
    */ 
    static Eigen::Vector3d RToEulerMin(const Eigen::Matrix3d& Rot);
    
  public:

    /**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskHEADING();

    /**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskHEADING();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */ 
    static void TaskErrorJac(const UAM::CHT& HT, const UAM::CArm& arm, const Eigen::MatrixXd& eepos_des, Eigen::MatrixXd& sigmaHEADING, Eigen::MatrixXd& JHEADING_reduced, Eigen::MatrixXd& JHEADING_pseudo_reduced);
};

} // End of UAM namespace

#endif
