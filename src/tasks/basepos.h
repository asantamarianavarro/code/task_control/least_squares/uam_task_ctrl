#ifndef _TASK_BASEPOS_H
#define _TASK_BASEPOS_H

// Own
#include <common_obj.h>

// Eigen
#include <eigen3/Eigen/Dense>

// Other
#include <math.h>
#include <cmath>

namespace UAM {

class CTaskBASEPOS
{
  private:

  public:

    /**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskBASEPOS();

    /**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskBASEPOS();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */ 
    static void TaskErrorJac(const UAM::CHT& HT, const UAM::CArm& arm, const Eigen::MatrixXd& pos_des, Eigen::MatrixXd& sigmaBASEPOS, Eigen::MatrixXd& JBASEPOS, Eigen::MatrixXd& JBASEPOS_pseudo);
    
    /**
    * \brief Rotation matrix to euler
    *
    * Returns the euler angles corresponding to the rotation matrix. Specifically returns the angles with the "shortest" path.
    * Based on http://www.staff.city.ac.uk/%7Esbbh653/publications/euler.pdf
    *
    */ 
    static Eigen::Vector3d RToEulerMin(const Eigen::Matrix3d& Rot);
};

} // End of UAM namespace

#endif
