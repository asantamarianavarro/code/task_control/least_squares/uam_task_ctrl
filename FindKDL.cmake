# Locate KDL install directory

MESSAGE(STATUS "Detecting KDL: using orocos_kdl-config.cmake to locate files.")

FIND_PATH(KDL_DIR orocos_kdl-config.cmake /usr/local/share/orocos_kdl)

INCLUDE (${KDL_DIR}/orocos_kdl-config.cmake)

IF (orocos_kdl_INCLUDE_DIRS AND orocos_kdl_LIBRARIES)
    SET(KDL_FOUND TRUE)
    MESSAGE(STATUS "   Libraries: ${orocos_kdl_LIBRARIES}")
    MESSAGE(STATUS "   Defines: ${orocos_kdl_INCLUDE_DIRS}")
ELSE (orocos_kdl_INCLUDE_DIRS AND orocos_kdl_LIBRARIES)
    SET(KDL_FOUND FALSE)
ENDIF (orocos_kdl_INCLUDE_DIRS AND orocos_kdl_LIBRARIES) 