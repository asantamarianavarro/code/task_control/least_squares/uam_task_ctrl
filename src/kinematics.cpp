#include "kinematics.h"

namespace UAM {

CKinematics::CKinematics(){}

CKinematics::~CKinematics(){}

void CKinematics::UAMKine(const UAM::CQuad& quad, UAM::CArm& arm, UAM::CHT& HT, UAM::CUAM& uam)
{
  // Get arm Homogenous transform, arm tip in base_link
  HT.tip_in_baselink = ArmFKine(arm);

  // Update Transforms
  HT.cam_in_baselink = HT.tip_in_baselink * HT.cam_in_tip;
  HT.tag_in_baselink = HT.cam_in_baselink * HT.tag_in_cam;

  HT.baselink_in_w = UAM::CCommon_Fc::GetTransform(quad.pos);
  HT.cam_in_w = HT.baselink_in_w * HT.cam_in_baselink;

  // Arm Jacobian
  Eigen::MatrixXd Ja = ArmJac(arm);

  // Quadrotor Jacobian body to inertial frames
  Eigen::MatrixXd Jb2i = QuadJac(quad.pos);

  // Inertial to body frames
  Eigen::MatrixXd Ji2b = UAM::CCommon_Fc::CalcPinv(Jb2i);

  // Jacobian from quadrotor body to camera
  Eigen::MatrixXd Jb2c = Eigen::MatrixXd::Zero(6,6);
  Jb2c.block(0,0,3,3) = Eigen::Matrix3d::Identity();
  Jb2c.block(0,3,3,3) = -UAM::CCommon_Fc::Skew(HT.cam_in_baselink.block(0,3,3,1));
  Jb2c.block(3,3,3,3) = Eigen::Matrix3d::Identity();

  // Rotation of camera in body frame
  Eigen::Matrix3d Rc_in_b = HT.cam_in_baselink.block(0,0,3,3);
  // Rotation of body in camera frame
  Eigen::Matrix3d Rb_in_c = Rc_in_b.transpose();

  // Transform Jacobian from body to camera frame
  Eigen::MatrixXd Rot = Eigen::MatrixXd::Zero(6,6);
  Rot.block(0,0,3,3)=Rb_in_c;
  Rot.block(3,3,3,3)=Rb_in_c;

  // Whole robot Jacobian
  uam.jacob = Eigen::MatrixXd::Zero(6,6+arm.nj);
  uam.jacob.block(0,0,6,6) = Rot*Jb2c*Ji2b;
  uam.jacob.block(0,6,6,arm.nj)=Rot*Ja;
  uam.vel = Eigen::MatrixXd::Zero(6+arm.nj,1);
}

Eigen::Matrix4d CKinematics::ArmFKine(UAM::CArm& arm)
{
  // constructs the kdl solver in non-realtime
  arm.jnt_to_pose_solver.reset(new KDL::ChainFkSolverPos_recursive(arm.chain));

  // Create joint array
  arm.jnt_pos_kdl = KDL::JntArray(arm.nj);

  // resize the joint state vector in non-realtime
  arm.jnt_pos_kdl.resize(arm.nj);

  for(unsigned int ii=0; ii < arm.nj; ii++)
    arm.jnt_pos_kdl.data(ii) = arm.jnt_pos(ii,0);

  // computes Cartesian pose in realtime From Base_link to Arm tip
  KDL::Frame kdlTarm;
  arm.jnt_to_pose_solver->JntToCart(arm.jnt_pos_kdl, kdlTarm);

  double roll,pitch,yaw;
  kdlTarm.M.GetEulerZYX(yaw,pitch,roll);

  Eigen::Matrix4d Tarm;
  Eigen::Matrix3d Rarm;
  // euler convention zyx
  Rarm = Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ()) \
       * Eigen::AngleAxisd(pitch, Eigen::Vector3d::UnitY()) \
     * Eigen::AngleAxisd(roll, Eigen::Vector3d::UnitX());

  Tarm.block(0,0,3,3) = Rarm;
  Tarm(0,3) = (double)kdlTarm.p.data[0];
  Tarm(1,3) = (double)kdlTarm.p.data[1];
  Tarm(2,3) = (double)kdlTarm.p.data[2];
  Tarm.row(3) << 0, 0, 0, 1;

  return Tarm;
}

bool CKinematics::ArmIKine(const UAM::CArm& arm, const Eigen::MatrixXd& joint_limits, const Eigen::MatrixXd& Ttarget, const Eigen::MatrixXd& q_pos, Eigen::MatrixXd& q_new)
{
  std::cout << "-> Starting Inverse kinematics computation..." << std::endl;

  // Create joint array
  KDL::JntArray jnt_pos = KDL::JntArray(arm.nj);

  // resizes the joint state vectors in non-realtime
  jnt_pos.resize(arm.nj);

  // Put current joint positions to kdl jnt_pos
  for(unsigned int i=0; i < arm.nj; i++)
    jnt_pos.data(i) = q_pos(i,0);

  //fk solver
  KDL::ChainFkSolverPos_recursive fksolver(KDL::ChainFkSolverPos_recursive(arm.chain));

  // Create joint array
  KDL::JntArray setpointJP = KDL::JntArray(arm.nj);
  KDL::JntArray max = KDL::JntArray(arm.nj); //The maximum joint positions
  KDL::JntArray min = KDL::JntArray(arm.nj); //The minimium joint positions

  for(unsigned int ii=0; ii < arm.nj; ii++)
  {
    min(ii) = joint_limits(ii,0);
    max(ii) = joint_limits(ii,1);
  }

  //Create inverse velocity solver
  KDL::ChainIkSolverVel_pinv_givens iksolverv = KDL::ChainIkSolverVel_pinv_givens(arm.chain);

  //Iksolver Position: Maximum 2000 iterations, stop at accuracy 1e-6
  //KDL::ChainIkSolverPos_NR iksolver = KDL::ChainIkSolverPos_NR(arm.chain,fksolver,iksolverv,100,1e-6);
  KDL::ChainIkSolverPos_NR_JL iksolver = KDL::ChainIkSolverPos_NR_JL(arm.chain, min, max, fksolver, iksolverv, 2000, 1e-6); //With Joints Limits

  KDL::Frame cartpos;
  KDL::JntArray jointpos = KDL::JntArray(arm.nj);

  // Copying translation to KDL frame
  for (size_t i = 0; i < 3; ++i)
  {
    cartpos.p(i) = Ttarget(i,3);
  }

  // Copying Rotation to KDL frame
  for (size_t j = 0; j < 3; ++j)
  {
    for (size_t i = 0; i < 3; ++i)
      cartpos.M(i,j) = Ttarget(i,j);
  }

  for(unsigned int i=0; i < arm.nj; i++)
    jnt_pos(i) = q_pos(i,0);

  // Calculate inverse kinematics to go from jnt_pos to cartpos. The result in jointpos
  int ret = iksolver.CartToJnt(jnt_pos, cartpos, jointpos);

  if (ret >= 0) 
  {
    std::cout << " Current Joint Position: [";
    for(unsigned int i=0; i < arm.nj; i++)
      std::cout << jnt_pos(i) << " ";
    std::cout << "]"<< std::endl;
    std::cout << "Cartesian Position " << cartpos << std::endl;
    std::cout << "IK result Joint Position: [";
    for(unsigned int i=0; i < arm.nj; i++)
      std::cout << jointpos(i) << " ";
    std::cout << "]"<< std::endl;

    q_new = Eigen::MatrixXd::Zero(arm.nj,1);

    for(unsigned int i=0; i < arm.nj; i++)
      q_new(i,0) = jointpos(i);
  }
  else
  {
    std::cout << "Error: could not calculate inverse kinematics :(" << std::endl;
    return false;
  }
  return true;
}

Eigen::MatrixXd CKinematics::ArmJac(UAM::CArm& arm)
{
  // constructs the kdl solver in non-realtime
  arm.jnt_to_jac_solver.reset(new KDL::ChainJntToJacSolver(arm.chain));

  // resize the joint state vector in non-realtime
  arm.jacobian.resize(arm.nj);

  // compute Jacobian in realtime
  arm.jnt_to_jac_solver->JntToJac(arm.jnt_pos_kdl, arm.jacobian);

  Eigen::MatrixXd Ja = Eigen::MatrixXd::Zero(6,arm.nj);

  for (unsigned int ii=0; ii<6; ++ii)
  {
    for (unsigned int jj=0; jj<arm.nj; ++jj)
      Ja(ii,jj) = arm.jacobian.data(ii,jj);
  }

  // TODO: Define the arm in order to avoid this.
  Ja.col(arm.nj-1) = -Ja.col(arm.nj-1);

  return Ja;
}

Eigen::MatrixXd CKinematics::QuadJac(const Eigen::MatrixXd& pos)
{
  double psi,theta,phi;

  phi = pos(3,0);
  theta = pos(4,0);
  psi = pos(5,0);
  //psi = 0;

  Eigen::MatrixXd Jq(6,6);
  Jq(0,0) = cos(psi)*cos(theta);
  Jq(0,1) = sin(phi)*cos(psi)*sin(theta)-cos(phi)*sin(psi);
  Jq(0,2) = cos(phi)*cos(psi)*sin(theta)+sin(phi)*sin(psi);
  Jq(1,0) = sin(psi)*cos(theta);
  Jq(1,1) = sin(phi)*sin(psi)*sin(theta)+cos(phi)*cos(psi);

  Jq(1,2) = cos(phi)*sin(psi)*sin(theta)-sin(phi)*cos(psi);
  Jq(2,0) = -sin(theta);
  Jq(2,1) = sin(phi)*cos(theta);
  Jq(2,2) = cos(phi)*cos(theta);
  Jq.block(0,3,3,3) = Eigen::MatrixXd::Zero(3,3);
  Jq.block(3,0,3,3) = Eigen::MatrixXd::Zero(3,3);
  Jq.block(3,3,3,3) = UAM::CCommon_Fc::GetWronskian(phi,theta);

  return Jq;
}

Eigen::MatrixXd CKinematics::ArmGetDH(const UAM::CArm& arm)
{
  // TODO: Not working properly yet.
  Eigen::MatrixXd DH(arm.chain.segments.size()-1,4);

  for (int ii = 1; ii < arm.chain.segments.size(); ++ii)
  {
    double a, alpha, d, theta;
    KDL::Frame fr = arm.chain.segments[ii].pose(arm.jnt_pos(ii-1,0));
    d = fr.p[2];
    alpha = acos(fr.M(2,2));
    theta = acos(fr.M(0,0));
    if (std::abs(fr.M(0,0)) > 1e-3)
      a = fr.p[0]/fr.M(0,0);
    else
      a = fr.p[1]/fr.M(1,0);
    DH.row(ii-1) << a, alpha, d, theta;
  }

  return DH;
}

} // End of UAM namespace
