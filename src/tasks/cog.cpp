// Own
#include "cog.h"
#include <common_fc.h>

// MISC
#include <iostream>      /* input, output operations */

// KDL stuff
#include <kdl/frames.hpp> 
#include <kdl/chain.hpp>

namespace UAM {

CTaskCOG::CTaskCOG(){}

CTaskCOG::~CTaskCOG(){}

void CTaskCOG::CoGCommon(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::Matrix3d& Rib, Eigen::MatrixXd& Jj_cog, UAM::ArmCogData& arm_cog_data)
{
  // KDL stuff
  std::vector<KDL::Frame> joint_pose(arm.nj); // Vector of Joint frames
  joint_pose.resize(arm.nj);

  // This should be done, otherwise an eigen assertion arises in the robot computer
  std::vector<Eigen::Matrix4d,Eigen::aligned_allocator<Eigen::Matrix4d> > Transf(arm.nj);
  //vector<Matrix4d> Transf(arm.nj); // Vector of HT of frames relative to each links
  Transf.resize(arm.nj);
  // Eigen stuff
  std::vector<Eigen::MatrixXd> T_base_to_joint(arm.nj+1); // Vector of HT of frame links relative to base
  T_base_to_joint.resize(arm.nj+1);
  std::vector<Eigen::MatrixXd> IIIrdcolRot_b_x(arm.nj+1); // Vector of r3d column of HT
  IIIrdcolRot_b_x.resize(arm.nj+1);
  std::vector<Eigen::MatrixXd> link_origin(arm.nj+1); // Origin of each link's frame
  link_origin.resize(arm.nj+1);

  // CoG data
  arm.mass = 0; // Total arm mass
  Eigen::MatrixXd arm_cg = Eigen::MatrixXd::Zero(4,1); // Sum of CoG vector
  arm_cog_data.arm_cog = Eigen::MatrixXd::Zero(3,1); // Arm CoG
  arm_cog_data.link_cog.resize(arm.nj);
  std::vector<Eigen::MatrixXd> link_cg(arm.nj); // Vector of link CoG relative to base
  link_cg.resize(arm.nj);

  // Get arm segments
  unsigned int num_seg = arm.chain.getNrOfSegments();
  std::vector<KDL::Segment> arm_segment(num_seg);
  arm_segment.resize(num_seg);
  for (unsigned int ii = 1; ii < num_seg; ++ii)
    arm_segment.at(ii) = arm.chain.getSegment(ii);

  // Initial transform
  T_base_to_joint.at(0) = HT.armbase_in_baselink;
  // 3rd column of rotation matrix
  IIIrdcolRot_b_x.at(0) = T_base_to_joint.at(0).block(0,2,3,1);
  // Origin of the initial frame
  link_origin.at(0) = T_base_to_joint.at(0).col(3);

  // Debug 
  arm.T_base_to_joint.resize(arm.nj+1);
  arm.T_base_to_joint.at(0) = T_base_to_joint.at(0);
  
  for (unsigned int jj = 0; jj < arm.nj; ++jj)
  {
    // Get mass of each link
    arm.joint_info.at(jj).mass = arm_segment.at(jj+1).getInertia().getMass();
    // Joint frames  
    joint_pose.at(jj) = arm_segment.at(jj+1).pose(arm.jnt_pos(jj,0));
    // relative Homogeneous transforms (HT)
    Transf.at(jj) = UAM::CCommon_Fc::GetTransform(joint_pose.at(jj));
    // HT r.t. base link
    T_base_to_joint.at(jj+1) = T_base_to_joint.at(jj) * Transf.at(jj);
    // 3rd column of rotation matrix
    IIIrdcolRot_b_x.at(jj+1) = T_base_to_joint.at(jj+1).inverse().block(0,2,3,1);
    // IIIrdcolRot_b_x.at(jj+1) = T_base_to_joint.at(jj+1).block(0,2,3,1);
    // Origin of the link's frame r.t. base_link
    link_origin.at(jj+1) = T_base_to_joint.at(jj+1).col(3);
    // Distance to the middle of the link jj
    Eigen::MatrixXd link_mid = (link_origin.at(jj+1)-link_origin.at(jj))/2;
    // Link CoG coordinates
    arm_cog_data.link_cog.at(jj) = link_origin.at(jj)+link_mid;
    // gravity for the link jj 
    link_cg.at(jj) = arm.joint_info.at(jj).mass * arm_cog_data.link_cog.at(jj);
    // Sum of link CoGs
    arm_cg = arm_cg + link_cg.at(jj);
    // Sum of link mass  
    arm.mass = arm.mass + arm.joint_info.at(jj).mass;

    //Debug
    // std::cout << "num segments: " << arm_segment.size() << std::endl;
    // std::cout << "Link: " << jj << " name:";
    // std::cout << arm_segment.at(jj+1).getName();
    // std::cout << " mass: " << arm.joint_info.at(jj).mass;
    // std::cout << " link_cog:" << std::endl;
    // std::cout << link_origin.at(jj)+arm_cog_data.cog_joint.at(jj)  << std::endl;
    // std::cout << "Transform: " << std::endl;
    // std::cout << T_base_to_joint.at(jj+1) <<std::endl;

    // std::cout << "______joint: " << jj << std::endl;
    // std::cout << ">> KDL: " << jj << std::endl;
    // std::cout << joint_pose.at(jj) << std::endl;
    // std::cout << "<< EIGEN matrix: " << jj << std::endl;
    // std::cout << Transf.at(jj) << std::endl;
    // std::cout << "trans: " << jj << std::endl;
    // std::cout << Transf.at(jj).block(0,3,3,1) << std::endl;
    // std::cout << "rot: " << jj << std::endl;
    // Matrix3d Rot = Transf.at(jj).block(0,0,3,3);
    // std::cout << Rot.eulerAngles(0,1,2) << std::endl;

    // Fill link information
    arm.T_base_to_joint.at(jj+1) = T_base_to_joint.at(jj+1);
  }

  // vector from arm base to CoG
  arm_cog_data.arm_cog = arm_cg/arm.mass;

  // Debug
  // std::cout << "mass: " << arm.mass << std::endl;
  // std::cout << "arm_cg: " << arm_cg << std::endl;
  // std::cout << "cog: " << arm_cog_data.arm_cog << std::endl;

  // Rotation between quadrotor body and inertial frames  
  Rib = HT.baselink_in_w.block(0,0,3,3)*HT.armbase_in_baselink.block(0,0,3,3);

  // X and Y values of CoG in quadrotor inertial frame
  cog_arm = arm_cog_data.arm_cog.block(0,0,3,1);
  Eigen::MatrixXd cog_arm_in_qi = Rib * cog_arm;
  cog_PGxy = cog_arm_in_qi.block(0,0,2,1);

  // Partial CoG and jacobian of each link (augmented links: from current to end-effector)
  Eigen::MatrixXd CoG_partial;
  for (unsigned int jj = 0; jj < arm.nj; ++jj)
  {
    // Get the center of gravity from the current link to the end effector
    double partial_mass = 0;
    Eigen::MatrixXd partial_arm_cg = Eigen::MatrixXd::Zero(4,1);
    for (unsigned int ii = jj; ii < arm.nj; ++ii)
    {
      partial_mass = partial_mass + arm.joint_info.at(ii).mass;
      partial_arm_cg = partial_arm_cg + link_cg.at(ii);
    }

    if(partial_mass!=0){
      CoG_partial = partial_arm_cg/partial_mass;
      Eigen::Matrix3d screw_rot = CCommon_Fc::Skew(IIIrdcolRot_b_x.at(jj));
      Jj_cog.col(jj) = (partial_mass/arm.mass)*screw_rot*CoG_partial.block(0,0,3,1);
    }
    else Jj_cog.col(jj) = Eigen::MatrixXd::Zero(3,1);
  }
}

void CTaskCOG::TaskErrorJacFull(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::MatrixXd& sigmaG, Eigen::MatrixXd& JG, Eigen::MatrixXd& JG_pseudo, UAM::ArmCogData& arm_cog_data)
{
  Eigen::Matrix3d Rib;
  Eigen::MatrixXd Jj_cog(3,arm.nj);
  CoGCommon(arm, HT, cog_PGxy, cog_arm, Rib, Jj_cog, arm_cog_data);
  // Task Error
  sigmaG = -cog_PGxy.array();

  // Task Jacobian
  JG = Eigen::MatrixXd::Zero(2,4+arm.nj);
  Eigen::Matrix3d Rb_in_w = HT.baselink_in_w.block(0,0,3,3);
  Eigen::Matrix3d Rarmb_in_b = HT.armbase_in_baselink.block(0,0,3,3);
  Eigen::Matrix3d Rl1_in_armb = HT.link1_in_armbase.block(0,0,3,3); 
  Eigen::Matrix3d skew_rot = UAM::CCommon_Fc::Skew(Rb_in_w*Rl1_in_armb*arm_cog_data.arm_cog.block(0,0,3,1));
  JG.block(0,3,2,1) = skew_rot.block(0,2,2,1);
  Eigen::MatrixXd Jacob_temp(3,arm.nj);
  for (unsigned int ii = 0; ii < arm.nj; ++ii)
    Jacob_temp.col(ii) = Rb_in_w * Rarmb_in_b * Rl1_in_armb * Jj_cog.col(ii);

  JG.block(0,4,2,arm.nj)=Jacob_temp.block(0,0,2,arm.nj);

  JG_pseudo = UAM::CCommon_Fc::CalcPinv(JG);
}

void CTaskCOG::TaskErrorJac(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::MatrixXd& sigmaG, Eigen::MatrixXd& JG, Eigen::MatrixXd& JG_pseudo, UAM::ArmCogData& arm_cog_data)
{
  Eigen::Matrix3d Rib;
  Eigen::MatrixXd Jj_cog(3,arm.nj);
  CoGCommon(arm, HT, cog_PGxy, cog_arm, Rib, Jj_cog, arm_cog_data);
  // Task Error
  sigmaG = -(cog_PGxy.transpose()*cog_PGxy);

  // Task Jacobian
  JG = Eigen::MatrixXd::Zero(1,4+arm.nj);
  Eigen::Matrix3d Rb_in_w = HT.baselink_in_w.block(0,0,3,3);
  Eigen::Matrix3d Rarmb_in_b = HT.armbase_in_baselink.block(0,0,3,3);
  Eigen::Matrix3d Rl1_in_armb = HT.link1_in_armbase.block(0,0,3,3); 
  Eigen::MatrixXd Jacob_temp(3,arm.nj);
  for (unsigned int ii = 0; ii < arm.nj; ++ii)
    Jacob_temp.col(ii) = Rb_in_w * Rarmb_in_b * Rl1_in_armb * Jj_cog.col(ii);

  JG.block(0,4,1,arm.nj)=2*cog_PGxy.transpose()*Jacob_temp.block(0,0,2,arm.nj);
  
  JG_pseudo = UAM::CCommon_Fc::CalcPinv(JG);
}

} // End of UAM namespace