#include <uam_task_ctrl.h>

#include <common_fc.h>

int main(int argc, char *argv[])
{
  // Test to check SVD
  std::cout << "...Running test..." << std::endl;
  
  Eigen::MatrixXd mm = Eigen::MatrixXd::Random(3,2);
  std::cout << "Here is the matrix m:" << std::endl << mm << std::endl;
  Eigen::JacobiSVD<Eigen::MatrixXd> svdd(mm, Eigen::ComputeThinU | Eigen::ComputeThinV);
  std::cout << "Its singular values are:" << std::endl << svdd.singularValues().transpose() << std::endl;
  std::cout << "Its left singular vectors are the columns of the thin U matrix:" << std::endl << svdd.matrixU() << std::endl;
  std::cout << "Its right singular vectors are the columns of the thin V matrix:" << std::endl << svdd.matrixV() << std::endl;
  Eigen::Vector3d rhs(1, 0, 0);
  std::cout << "Now consider this rhs vector:" << std::endl << rhs.transpose() << std::endl;
  std::cout << "A least-squares solution of m*x = rhs is:" << std::endl << svdd.solve(rhs).transpose() << std::endl;
  
  std::cout << "Launching CalcPinv from main library" << std::endl;
  
  Eigen::MatrixXd mmi = UAM::CCommon_Fc::CalcPinv(mm);
  
  std::cout << "Test done." << std::endl;
}

