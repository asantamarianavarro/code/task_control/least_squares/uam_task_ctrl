#edit the following line to add the librarie's header files
FIND_PATH(uam_task_ctrl_INCLUDE_DIR uam_task_ctrl.h /usr/local/include/iridrivers/uam)

FIND_LIBRARY(uam_task_ctrl_LIBRARY
    NAMES uam_task_ctrl
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers) 

IF (uam_task_ctrl_INCLUDE_DIR AND uam_task_ctrl_LIBRARY)
   SET(uam_task_ctrl_FOUND TRUE)
ENDIF (uam_task_ctrl_INCLUDE_DIR AND uam_task_ctrl_LIBRARY)

IF (uam_task_ctrl_FOUND)
  IF (NOT uam_task_ctrl_FIND_QUIETLY)
    MESSAGE(STATUS "Found uam_task_ctrl: ${uam_task_ctrl_LIBRARY}")
  ENDIF (NOT uam_task_ctrl_FIND_QUIETLY)
ELSE (uam_task_ctrl_FOUND)
  IF (uam_task_ctrl_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find uam_task_ctrl")
  ENDIF (uam_task_ctrl_FIND_REQUIRED)
ENDIF (uam_task_ctrl_FOUND)

