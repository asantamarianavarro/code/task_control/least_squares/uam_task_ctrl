// Own
#include "basepos.h"
#include "common_fc.h"
#include "kinematics.h"

// MISC
#include <iostream>      /* input, output operations */
 
namespace UAM {

CTaskBASEPOS::CTaskBASEPOS(){}

CTaskBASEPOS::~CTaskBASEPOS(){}

Eigen::Vector3d CTaskBASEPOS::RToEulerMin(const Eigen::Matrix3d& Rot)
{
  Eigen::Vector3d ang;

  if (Rot(2,0)!=-1.0 && Rot(2,0)!=1.0)
  {
    double phi1, theta1, psi1;
    double phi2, theta2, psi2;

    theta1 = -asin(Rot(2,0));
    theta2 = 3.141592653589793 - theta1;
    psi1 = atan2(Rot(2,1)/cos(theta1),Rot(2,2)/cos(theta1)); 
    psi2 = atan2(Rot(2,1)/cos(theta2),Rot(2,2)/cos(theta2)); 
    phi1 = atan2(Rot(1,0)/cos(theta1),Rot(0,0)/cos(theta1)); 
    phi2 = atan2(Rot(1,0)/cos(theta2),Rot(0,0)/cos(theta2)); 

    // TODO: This statement can cause big discontinuities in the obtained angles when 
    // the two norms are similar.
    if (sqrt(std::pow(theta1,2)+std::pow(psi1,2)+std::pow(phi1,2)) < sqrt(std::pow(theta2,2)+std::pow(psi2,2)+std::pow(phi2,2)))
      ang << psi1, theta1, phi1;
    else
      ang << psi2, theta2, phi2; // ZYX Euler convention.
  }
  else
  {
    double phi, theta, psi;
    phi = 0.0;
    if (Rot(2,0)==-1.0)
    {
      theta = 3.141592653589793/2;
      psi = phi + atan2(Rot(0,1),Rot(0,2)); 
    }
    else
    {
      theta = -3.141592653589793/2;
      psi = -phi + atan2(-Rot(0,1),-Rot(0,2)); 
    }
    ang << phi, theta, psi;
  }

  return ang;
}

void CTaskBASEPOS::TaskErrorJac(const UAM::CHT& HT, const UAM::CArm& arm, const Eigen::MatrixXd& pos_des, Eigen::MatrixXd& sigmaBASEPOS, Eigen::MatrixXd& JBASEPOS, Eigen::MatrixXd& JBASEPOS_pseudo)
{
  // Initialize sizes
  sigmaBASEPOS = Eigen::MatrixXd::Zero(4,1);
  
  // Task Error ________________________________________________

  // End-Effector Position Error 
  sigmaBASEPOS.block(0,0,3,1) = pos_des.block(0,0,3,1) - HT.baselink_in_w.block(0,3,3,1);

  // End-Effector Orientation Error
  Eigen::Vector3d angles = RToEulerMin(HT.baselink_in_w.block(0,0,3,3));
  sigmaBASEPOS(3,0) = pos_des(3,0) - angles(2,0);

  // Task Jacobian _____________________________________________
  JBASEPOS = Eigen::MatrixXd::Zero(pos_des.rows(),4+arm.nj);
  JBASEPOS.block(0,0,4,4) = Eigen::Matrix4d::Identity();
  JBASEPOS_pseudo = UAM::CCommon_Fc::CalcPinv(JBASEPOS);
}

} // End of UAM namespace
