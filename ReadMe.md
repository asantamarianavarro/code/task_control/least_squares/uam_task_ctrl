# C++ library for Task Control of Unmanned Aerial Manipulators (UAM)

This library provides the robot kinematics as well as a framework to compute several task errors and Jacobians. 

**Related publications:** 

[Uncalibrated Visual Servo for Unmanned Aerial Manipulation](http://www.angelsantamaria.eu/publications/tmech17)

A. Santamaria-Navarro, P. Grosch, V. Lippiello, J. Solà and J. Andrade-Cetto

IEEE/ASME Transactions on Mechatronics. To appear. 

### Software dependencies

- [Boost](http://www.boost.org/) (already installed in most Ubuntu LTS)
  
- [Orocos - KDL](http://www.orocos.org/kdl)
  
  - WARN: Orocos-KDL standalone library can conflict with ROS orocos-kdl version. Tested with orocos-kdl-1.3.0. To install it follow these steps:
    
        - `cd ~/Downloads && wget https://github.com/orocos/orocos_kinematics_dynamics/archive/1.3.0.tar.gz && tar -xvzf 1.3.0.tar.gz && rm 1.3.0.tar.gz`
        - `cd orocos_kinematics_dynamics-1.3.0/orocos_kdl && mkdir build && cd build`
        - `cmake .. && make -j $(nproc) && sudo make install`

- [Eigen 3](eigen.tuxfamily.org)

### Installation

- Clone the repository: `git clone https://gitlab.iri.upc.edu/asantamaria/uam_task_ctrl.git`
- Install:	`cd uam_task_ctrl/build && cmake -D CMAKE_BUILD_TYPE=RELEASE .. && make -j $(nproc) && sudo make install`

### Example of usage

- Run `/bin/./uam_task_ctrl_test`.

- Checkout [kinton_arm_task_priority_control](https://gitlab.iri.upc.edu/asantamaria/kinton_arm_task_priority_control) project to use this library within ROS framework.

### Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)
