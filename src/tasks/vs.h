#ifndef _TASK_VS_H
#define _TASK_VS_H

// Eigen
#include <eigen3/Eigen/Dense>

namespace UAM {

class CTaskVS
{
  private:

  public:

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskVS();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskVS();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */ 
    static void TaskErrorJac(const int& arm_dof, const Eigen::MatrixXd& vs_delta_gain, const double& vs_alpha_min, const double& target_dist, const Eigen::MatrixXd& v_rollpitch, const Eigen::MatrixXd& cam_vel, const Eigen::MatrixXd& uam_jac, Eigen::MatrixXd& sigmaVS, Eigen::MatrixXd& JVS, Eigen::MatrixXd& JVS_pseudo);
};

} // End of UAM namespace

#endif
