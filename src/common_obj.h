
#ifndef _COMMON_OBJ_H
#define _COMMON_OBJ_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string>
#include <sstream>
#include <math.h>

// Eigen
#include <eigen3/Eigen/Dense>

// KDL
#include <kdl/chain.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>

// Boost
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

namespace UAM {

// Arm joint
class CArmJoint{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    std::string link_parent; // Link parent name.
    std::string link_child;  // Link child name.
    double joint_min;        // Joint lower limit values.
    double joint_max;        // Joint upper limit values.
    double mass;             // Joint mass.
};

// Arm CoG data
class ArmCogData{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd> > link_cog; // Vector of link CoG relative to base 
    Eigen::MatrixXd arm_cog;               // Arm CoG coordinates w.r.t. arm base link;
};

// Arm
class CArm{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    KDL::Chain chain;                                                       // KDL chain.
    double mass;                                                            // mass.
    unsigned int nj;                                                        // number of joints.
    std::vector<CArmJoint> joint_info;                                      // joints info.
    Eigen::MatrixXd jnt_pos;                                                // Joint value.
    boost::scoped_ptr<KDL::ChainFkSolverPos_recursive> jnt_to_pose_solver;  // chain solver.
    boost::scoped_ptr<KDL::ChainJntToJacSolver> jnt_to_jac_solver;          // chain solver.
    KDL::JntArray jnt_pos_kdl;                                              // Array of arm positions.
    KDL::Jacobian jacobian;                                                 // Jacobian.
    std::vector<Eigen::MatrixXd, Eigen::aligned_allocator<Eigen::MatrixXd> > T_base_to_joint;                           // Homogenous Transforms from arm base to each link.
    CArm(){};
    ~CArm(){};
};

// Quadrotor
class CQuad{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::MatrixXd pos;  // position values (joints).
};

// UAM (Unmanned Aerial Manipulator)
class CUAM{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::MatrixXd vel;   // Velocities (joints).
    Eigen::MatrixXd jacob; // Jacobian.
};

// Homogenous Transforms
class CHT{
  public:  
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::Matrix4d tag_in_cam;          // Tag in camera frames.
    Eigen::Matrix4d tag_in_baselink;     // Tag in base_link frames.
    Eigen::Matrix4d cam_in_tip;          // Camera in arm tip frames.
    Eigen::Matrix4d cam_in_baselink;     // Camera in base_link frames.
    Eigen::Matrix4d tip_in_baselink;     // Arm forward kinematics (from base_link to arm link frame).
    Eigen::Matrix4d armbase_in_baselink; // Camera in base_link frames.
    Eigen::Matrix4d link1_in_armbase;    // Arm link 1 in arm base frame.
    Eigen::Matrix4d baselink_in_w;       // base_link in World frames.
    Eigen::Matrix4d cam_in_w;            // Camera in World frames.

    CHT(){
      tag_in_cam = Eigen::Matrix4d::Identity();
      tag_in_baselink = Eigen::Matrix4d::Identity();
      cam_in_tip = Eigen::Matrix4d::Identity();
      cam_in_baselink = Eigen::Matrix4d::Identity();
      tip_in_baselink = Eigen::Matrix4d::Identity();
      armbase_in_baselink = Eigen::Matrix4d::Identity();
      link1_in_armbase = Eigen::Matrix4d::Identity();
      baselink_in_w = Eigen::Matrix4d::Identity();
      cam_in_w = Eigen::Matrix4d::Identity();
    };    
    ~CHT(){};    
};

// Control Law parameters
class CCtrlParams{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    bool enable_sec_task;             // Enable secondary task.
    double dt;                        // time interval.
    double stabil_gain;               // Gain of kinematics stabilization secondary task.
    Eigen::MatrixXd lambda_robot;     // Robot proportional gains.
    Eigen::MatrixXd ir_vel;           // Inflation radius velocity.      
    Eigen::MatrixXd vs_vel;           // Primary task velocity.      
    double vs_alpha_min;              // Alpha value for gain matrix pseudo inverse.
    Eigen::MatrixXd vs_delta_gain;    // Delta values (min and max) for gain matrix pseudo inverse W.
    double ir_gain;                   // Gain of Inflation radius task.
    double inf_radius;                // Security distance to an obstacle (Inflation radius) to prevent Collisions.
    double cog_gain;                  // Gain of CoG alignment secondary task.
    Eigen::MatrixXd cog_vel;          // Secondary task velocity.
    Eigen::MatrixXd cog_PGxy;         // X and Y of the CoG r.t. Quadrotor body inertial frame.
    Eigen::MatrixXd cog_arm;          // Arm Center of Gravity r.t. quadrotor body frame.
    double jntlim_gain;               // Gain of joint limits secondary task.
    Eigen::MatrixXd jntlim_vel;       // Joint limit task velocity.
    Eigen::MatrixXd jntlim_pos_des;   // Desired arm position for secondary task (avoiding joint limits).
    Eigen::MatrixXd jntlim_pos_error; // Arm joint errors between current and secondary task desired joint positions
    Eigen::MatrixXd q_rollpitch;      // Quadrotor roll and pitch angles.
    Eigen::MatrixXd v_rollpitch;      // Quadrotor roll and pitch angular velocities.
};

// Task Eigen Values
class TaskEigVal{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::VectorXd vs;	// Visual Servo Task Eigen Values
    Eigen::VectorXd cog;  // CoG Task Eigen Values
    Eigen::VectorXd jl;   // Joint Limits Task Eigen Values
};

// Task Jacobians
class TaskJac{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    Eigen::MatrixXd ir;  // Inflation Radius Task Jac
    Eigen::MatrixXd vs;  // Visual Servo Task Jac
    Eigen::MatrixXd cog; // CoG Task Jac
    Eigen::MatrixXd jl;  // Joint Limits Task Jac
};

} // End of UAM namespace

#endif
