#ifndef _TASK_JL_H
#define _TASK_JL_H

// Eigen
#include <eigen3/Eigen/Dense>

// Own
#include <common_obj.h>

namespace UAM {

class CTaskJL
{
  private:

    /**
    * \brief JL Jacobian and common data computation.
    *
    * Returns the JL Jacobian and JL data used in other functions.
    *
    */  
    static Eigen::MatrixXd TaskErrorCommon(const UAM::CArm& arm, const double& pow_n, const Eigen::MatrixXd& jntlim_pos_des, Eigen::MatrixXd& jntlim_pos_error, Eigen::MatrixXd& AAL);

  public:

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskJL();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskJL();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */  
    static void TaskErrorJac(UAM::CArm& arm, const Eigen::MatrixXd& jntlim_pos_des, Eigen::MatrixXd& jntlim_pos_error, Eigen::MatrixXd& sigmaL, Eigen::MatrixXd& JL, Eigen::MatrixXd& JL_pseudo);

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    * The returned Jacobian has full size (Arm DoF rows) and similarly the pseudo inverse of the jacobian.
    *
    */  
    static void TaskErrorJacFull(UAM::CArm& arm, const Eigen::MatrixXd& jntlim_pos_des, Eigen::MatrixXd& jntlim_pos_error, Eigen::MatrixXd& sigmaL, Eigen::MatrixXd& JL, Eigen::MatrixXd& JL_pseudo);
};

} // End of UAM namespace

#endif
