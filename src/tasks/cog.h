#ifndef _TASK_COG_H
#define _TASK_COG_H

// Eigen
#include <eigen3/Eigen/Dense>

// Own
#include <common_obj.h>
#include <common_fc.h>

namespace UAM {

class CTaskCOG
{
  private:

    /**
    * \brief CoG Jacobian and common data computation.
    *
    * Returns the CoG Jacobian and CoG data used in other functions.
    *
    */    
    static void CoGCommon(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::Matrix3d& Rib, Eigen::MatrixXd& Jj_cog, UAM::ArmCogData& arm_cog_data);

  public:    

    /**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskCOG();

    /**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskCOG();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */    
    static void TaskErrorJac(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::MatrixXd& sigmaG, Eigen::MatrixXd& JG, Eigen::MatrixXd& JG_pseudo, UAM::ArmCogData& arm_cog_data);

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations. 
    * The returned Jacobian has full size (2 rows) and similarly the pseudo inverse of the jacobian.
    *
    */  
    static void TaskErrorJacFull(UAM::CArm& arm, const UAM::CHT& HT, Eigen::MatrixXd& cog_PGxy, Eigen::MatrixXd& cog_arm, Eigen::MatrixXd& sigmaG, Eigen::MatrixXd& JG, Eigen::MatrixXd& JG_pseudo, UAM::ArmCogData& arm_cog_data);
};

} // End of UAM namespace

#endif
