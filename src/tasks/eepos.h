#ifndef _TASK_EEPOS_H
#define _TASK_EEPOS_H

// Own
#include <common_obj.h>

// Eigen
#include <eigen3/Eigen/Dense>

// Other
#include <math.h>
#include <cmath>

namespace UAM {

class CTaskEEPOS
{
  private:

  public:

    /**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CTaskEEPOS();

    /**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CTaskEEPOS();

    /**
    * \brief Task Error and Jacobians
    *
    * Returns the task error and Jacobians. Both error and Jacobians are computed in the same functions to avoid repeated operations.
    *
    */ 
    static void TaskErrorJac(const UAM::CHT& HT, UAM::CArm& arm, const Eigen::MatrixXd& vs_delta_gain, const double& vs_alpha_min, const Eigen::MatrixXd& eepos_des, Eigen::MatrixXd& sigmaEEPOS, Eigen::MatrixXd& JEEPOS_reduced, Eigen::MatrixXd& JEEPOS_pseudo_reduced);

    /**
    * \brief Rotation matrix to euler
    *
    * Returns the euler angles corresponding to the rotation matrix. Specifically returns the angles with the "shortest" path.
    * Based on http://www.staff.city.ac.uk/%7Esbbh653/publications/euler.pdf
    *
    */ 
    static Eigen::Vector3d RToEulerMin(const Eigen::Matrix3d& Rot);
    static Eigen::Vector3d RToEulerContinuous(const Eigen::Matrix3d& Rot, const Eigen::Vector3d& rpy_old);
};

} // End of UAM namespace

#endif
