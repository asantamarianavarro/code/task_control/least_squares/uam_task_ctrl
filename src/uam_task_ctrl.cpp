#include "uam_task_ctrl.h"

// Tasks specific
#include <tasks/ir.h>
#include <tasks/vs.h>
#include <tasks/cog.h>
#include <tasks/jl.h>
#include <tasks/eepos.h>

using namespace Eigen;
using namespace KDL;
using namespace std;

CHierarchTaskPCtrl::CHierarchTaskPCtrl()
{
  // this->datetime_ = UAM::CCommon_Fc::get_datetime();
}
 
CHierarchTaskPCtrl::~CHierarchTaskPCtrl()
{
}

// Main public function
void CHierarchTaskPCtrl::htpc(const MatrixXd& quad_dist_obs,
                              const goal_obj& goal,
                              UAM::CHT& HT,
                              UAM::CArm& arm,
                              const UAM::CQuad& quad,
                              UAM::CCtrlParams& ctrl_params,
                              MatrixXd& robot_pos,
                              MatrixXd& robot_vel)
{
  // Current quadrotor height
  this->quad_dist_obs_ = quad_dist_obs;

  // Goal related objects
  this->cam_vel_ = goal.cam_vel;
  this->target_dist_ = goal.target_dist;
  // Homogeneous transformations
  this->T_ = HT;
  // Control parameters
  this->ctrl_params_ = ctrl_params;
  // Arm data
  this->arm_.chain = arm.chain;
  this->arm_.joint_info= arm.joint_info;
  this->arm_.jnt_pos = arm.jnt_pos;
  this->arm_.nj = this->arm_.chain.getNrOfJoints();
  // Quad data
  this->quad_.pos = quad.pos;

  UAM::CKinematics::UAMKine(this->quad_,this->arm_,this->T_,this->uam_);
  uam_hierarchical_ctrl();

  // Arm positions increment
  //this->arm_.jnt_pos = this->arm_.jnt_pos+(this->uam_.vel.block(6,0,this->arm_.nj,1) * this->ctrl_params_.dt);

  // Quadrotor positions increment
  //this->quad_.pos = this->quad_.pos+(this->uam_.vel.block(0,0,6,1) * this->ctrl_params_.dt);

  // return values
  HT = this->T_; // DEBUG. TODO: make it constant and do not use this trans. outside.
  arm.T_base_to_joint = this->arm_.T_base_to_joint; // DEBUG. TODO: make it constant and do not use this trans. outside.
  ctrl_params = this->ctrl_params_;
  robot_pos.block(0,0,6,1) = this->quad_.pos;
  robot_pos.block(6,0,this->arm_.nj,1) = this->arm_.jnt_pos;
  robot_vel = this->uam_.vel;
}

// Control
void CHierarchTaskPCtrl::uam_hierarchical_ctrl()
{
  // TASK 0: Security task (Inflation radius) _____________________
 
  // Task error and Jacobians 
  MatrixXd JIR,JIR_pseudo,sigmaIR;
  UAM::CTaskIR::TaskErrorJac(this->arm_.nj,this->ctrl_params_.inf_radius,this->quad_dist_obs_,sigmaIR,JIR,JIR_pseudo);

  // Gain
  double lambdaIR = this->ctrl_params_.ir_gain;

  // task velocity
  this->ctrl_params_.ir_vel = JIR_pseudo * lambdaIR * sigmaIR;

  // TASK 1: Visual Servo _________________________________________
  
  // Task error and Jacobians 
  MatrixXd JVS,JVS_wpseudo,sigmaVS;
  UAM::CTaskVS::TaskErrorJac(this->arm_.nj,this->ctrl_params_.vs_delta_gain,this->ctrl_params_.vs_alpha_min,this->target_dist_,this->ctrl_params_.v_rollpitch,this->cam_vel_,this->uam_.jacob,sigmaVS,JVS,JVS_wpseudo);

  // Null space projector
  MatrixXd eye = MatrixXd::Identity(4+this->arm_.nj,4+this->arm_.nj);
  MatrixXd NIR = (eye-(JIR_pseudo*JIR));

  // task velocity
  this->ctrl_params_.vs_vel = NIR * JVS_wpseudo * sigmaVS;

  // TASK 2: CoG alignement ______________________________________

  // Null space projector
  MatrixXd JIR_VS = MatrixXd::Zero(JIR.rows()+JVS.rows(),JVS.cols());
  JIR_VS.block(0,0,JIR.rows(),JIR.cols()) = JIR;
  JIR_VS.block(JIR.rows()-1,0,JVS.rows(),JVS.cols())=JVS; 
  MatrixXd NIR_VS = MatrixXd::Zero(4+this->arm_.nj,4+this->arm_.nj);
  MatrixXd JIR_VS_pseudo = UAM::CCommon_Fc::CalcPinv(JIR_VS);
  NIR_VS = (eye-(JIR_VS_pseudo*JIR_VS));

  // Task error and Jacobians 
  MatrixXd JG,JG_pseudo,sigmaG;
  UAM::CTaskCOG::TaskErrorJac(this->arm_,this->T_,this->ctrl_params_.cog_PGxy,this->ctrl_params_.cog_arm,sigmaG,JG,JG_pseudo,this->arm_cog_data_);

  // Gain
  double lambdaG = this->ctrl_params_.cog_gain;

  // task velocity
  this->ctrl_params_.cog_vel = NIR_VS * JG_pseudo * lambdaG * sigmaG;

  // TASK 3: Joint limits ________________________
  
  // Augmented null space projector from Augmented Jacobian
  MatrixXd JIR_VS_G = MatrixXd::Zero(JIR_VS.rows()+JG.rows(),JIR_VS.cols());
  JIR_VS_G.block(0,0,JIR_VS.rows(),JIR_VS.cols()) = JIR_VS;
  JIR_VS_G.block(JIR_VS.rows()-1,0,JG.rows(),JG.cols())=JG; 
  MatrixXd NIR_VS_G = MatrixXd::Zero(4+this->arm_.nj,4+this->arm_.nj);
  MatrixXd JIR_VS_G_pseudo = UAM::CCommon_Fc::CalcPinv(JIR_VS_G);

  // Third task after secondary aligning CoG
  NIR_VS_G = (eye-(JIR_VS_G_pseudo*JIR_VS_G));

  // task Jacobian and sigma
  MatrixXd JL,JL_pseudo,sigmaL;
  UAM::CTaskJL::TaskErrorJac(this->arm_, this->ctrl_params_.jntlim_pos_des, this->ctrl_params_.jntlim_pos_error, sigmaL, JL, JL_pseudo);
  
  // Gain
  double lambdaL = this->ctrl_params_.jntlim_gain;

  // task velocity
  //this->ctrl_params_.jntlim_vel = NIR_VS * JL_pseudo * lambdaL * sigmaL; // As secondary
  this->ctrl_params_.jntlim_vel = NIR_VS_G * JL_pseudo * lambdaL * sigmaL;

  // Total velocities _______________________________

  //******************************************************************
  // Weighted sum of subtasks
  // this->ctrl_params_.cog_vel = NIR * (JG_pseudo * lambdaG * sigmaG + JL_pseudo * lambdaL * sigmaL);
  // this->ctrl_params_.jntlim_vel = MatrixXd::Zero(9,1);

  //******************************************************************
  //Total velocity __________________________________________
  if (!this->ctrl_params_.enable_sec_task) 
  {
    this->ctrl_params_.cog_vel = MatrixXd::Zero(4+this->arm_.nj,1);
    this->ctrl_params_.jntlim_vel = MatrixXd::Zero(4+this->arm_.nj,1);
    cout << "[Task Priority Control]: Secondary tasks not enabled" << endl;
  }

  MatrixXd Vtotal(4+this->arm_.nj,1);

  // Task 0 + 1 + 2
  //Vtotal = this->ctrl_params_.ir_vel + this->ctrl_params_.vs_vel + this->ctrl_params_.cog_vel; 
  // Task 0 + 1 + 3
  //Vtotal = this->ctrl_params_.ir_vel + this->ctrl_params_.vs_vel + this->ctrl_params_.jntlim_vel; 
  // All tasks
  Vtotal = this->ctrl_params_.ir_vel + this->ctrl_params_.vs_vel + this->ctrl_params_.cog_vel + this->ctrl_params_.jntlim_vel; 

  Vtotal = this->ctrl_params_.lambda_robot.array()*Vtotal.array();

  // // Debug
  // std::cout << "Task velocities" << std::endl;
  // std::cout << this->ctrl_params_.ir_vel.transpose() << std::endl;
  // std::cout << this->ctrl_params_.vs_vel.transpose() << std::endl;
  // std::cout << this->ctrl_params_.cog_vel.transpose() << std::endl;
  // std::cout << this->ctrl_params_.jntlim_vel.transpose() << std::endl;

  // Check singular configurations 
  if (isnan(Vtotal(0,0)))
    Vtotal=MatrixXd::Zero(4+this->arm_.nj,1);

  // Rearranging terms
  this->uam_.vel = MatrixXd::Zero(6+this->arm_.nj,1);
  this->uam_.vel.block(0,0,3,1) = Vtotal.block(0,0,3,1);
  this->uam_.vel.block(5,0,1+this->arm_.nj,1) = Vtotal.block(3,0,1+this->arm_.nj,1);

  // Eigenvalues check: Singular values (the positive square roots of the nonzero eigenvalues of the corresponding matrix J^T*J)
  MatrixXd Jvssq = JVS.transpose()*JVS;
  MatrixXd Jgsq = JG.transpose()*JG;
  MatrixXd Jlsq = JL.transpose()*JL;
  this->eig_values_.vs = Jvssq.eigenvalues().real();
  this->eig_values_.cog = Jgsq.eigenvalues().real()*1e6;
  this->eig_values_.jl = Jlsq.eigenvalues().real();
  // std::cout << "***********" << std::endl << std::endl;
  // std::cout << "VS eigenvalues:" << std::endl << this->eig_values_.vs << std::endl;
  // std::cout << "CoG eigenvalues:" << std::endl << this->eig_values_.cog << std::endl;
  // std::cout << "JL eigenvalues:" << std::endl << this->eig_values_.jl << std::endl;

  // Storing Jacobians to use them externally
  this->jacobians_.ir = JIR;
  this->jacobians_.vs = JVS;
  this->jacobians_.cog = JG;
  this->jacobians_.jl = JL;
}
