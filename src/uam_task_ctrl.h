#ifndef _UAM_TASK_CTRL_H
#define _UAM_TASK_CTRL_H

// MISC
#include <iostream>
#include <string>

// Eigen
#include <eigen3/Eigen/Dense>

// Own libraries
#include "common_fc.h"
#include "common_obj.h"
#include "kinematics.h"

using namespace Eigen;
using namespace KDL;
using namespace std;

// Goal related objects
typedef struct{
	MatrixXd cam_vel; // Desired camera velocity (end-effector)
    double target_dist; // Target distance
}goal_obj;

class CHierarchTaskPCtrl
{
  private:

    UAM::CUAM uam_;   // Unmanned Aerial Manipulator.
    UAM::CArm arm_;   // Arm.
    UAM::CQuad quad_; // Quadrotor.

    UAM::CCtrlParams ctrl_params_; // Control Law parameters.

    UAM::CHT T_; // Homogeneous Transforms.

    MatrixXd cam_vel_; // Camera velocities.

    double target_dist_; // Euclidean distance to target.

    MatrixXd quad_dist_obs_; // Quadrotor distance to obstacle.

    // string datetime_; // Initial date and time.

    /**
    * \brief Compute the 9DOF control law
    *
    * Compute the motions of the quadrotor and arm joints
    *
    */ 
    void uam_hierarchical_ctrl();

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    //TODO: these variables are public for debugging purposes
    // they must be private and set get functions created.
    UAM::ArmCogData arm_cog_data_; // Arm CoG data to debug in higher levels.

    UAM::TaskEigVal eig_values_; // Task jacobian eigenvalues.

    UAM::TaskJac jacobians_; // Task Jacobians.   

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CHierarchTaskPCtrl();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CHierarchTaskPCtrl();

    /**
    * \brief Hierarchical Task Priority Control Main API Function
    *
    * Main public function call of Quadarm Task Priority Control.
    *
    */     
    void htpc(const MatrixXd& quad_dist_obs,
              const goal_obj& goal,
              UAM::CHT& HT,
              UAM::CArm& arm,
              const UAM::CQuad& quad,
              UAM::CCtrlParams& CtrlParams,
              MatrixXd& robot_pos,
              MatrixXd& robot_vel);
};

#endif
