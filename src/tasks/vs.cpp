// Own
#include "vs.h"
#include <common_fc.h>

// MISC
#include <iostream>      /* input, output operations */

namespace UAM {

CTaskVS::CTaskVS(){}

CTaskVS::~CTaskVS(){}

void CTaskVS::TaskErrorJac(const int& arm_dof, const Eigen::MatrixXd& vs_delta_gain, const double& vs_alpha_min, const double& target_dist, const Eigen::MatrixXd& v_rollpitch, const Eigen::MatrixXd& cam_vel, const Eigen::MatrixXd& uam_jac, Eigen::MatrixXd& sigmaVS, Eigen::MatrixXd& JVS, Eigen::MatrixXd& JVS_pseudo)
{
  // Underactuated Quadrotor 
  // 9 DOF Extracting wx and wy from quadrotor 
  Eigen::MatrixXd J1(8,4),J2(8,2),Jqa1(6,4+arm_dof);
  Jqa1.block(0,0,6,3) = uam_jac.block(0,0,6,3);
  Jqa1.block(0,3,6,1+arm_dof) = uam_jac.block(0,5,6,1+arm_dof);
  Eigen::MatrixXd Jqa2 = uam_jac.block(0,3,6,2);
  Jqa2 = uam_jac.block(0,3,6,2);

  // task velocity vector
  sigmaVS = cam_vel-Jqa2*v_rollpitch;

  // task jacobian
  JVS = Jqa1;

  // task jacobian pseudo inverse 
  // JVS_pseudo = UAM::CCommon_Fc::CalcPinv(JVS);
  JVS_pseudo = UAM::CCommon_Fc::WeightInvJac(JVS,vs_delta_gain,vs_alpha_min,target_dist);
}

} // End of UAM namespace