#ifndef _KINEMATICS_H
#define _KINEMATICS_H

// Eigen
#include <eigen3/Eigen/Dense>

// KDL stuff
#include <kdl/frames.hpp> 
#include <kdl/frames_io.hpp> 
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolvervel_pinv_givens.hpp>
#include <kdl/chainjnttojacsolver.hpp>

// Own stuff
#include "common_obj.h"
#include "common_fc.h"

namespace UAM {

class CKinematics
{
  private:

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CKinematics();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CKinematics();

    /**
    * \brief Compute Arm Forward Kinematics
    *
    * Compute the arm Forward Kinematics
    *
    */      
    static Eigen::Matrix4d ArmFKine(UAM::CArm& arm);

    /**
    * \brief Compute Arm Forward Inverse Kinematics
    *
    * Compute the arm Forward Inverse Kinematics
    *
    */  
    static bool ArmIKine(const UAM::CArm& arm, const Eigen::MatrixXd& joint_limits, const Eigen::MatrixXd& Ttarget, const Eigen::MatrixXd& q_pos, Eigen::MatrixXd& q_new);

    /**
    * \brief Compute Arm Jacobian
    *
    * Compute the arm Jacobian
    *
    */  
    static Eigen::MatrixXd ArmJac(UAM::CArm& arm);
    
    /**
    * \brief Compute Arm Denavit-Hartenberg parameters
    *
    * Compute Arm Denavit-Hartenberg parameters. Returns Matrix with DH params: a, alpha, d, theta.
    *
    */  
    static Eigen::MatrixXd ArmGetDH(const UAM::CArm& arm);
      
    /**
    * \brief Compute Quadrotor Jacobian
    *
    * Compute the quadrotor Jacobian
    *
    */  
    static Eigen::MatrixXd QuadJac(const Eigen::MatrixXd& pos);

    /**
    * \brief Unmanned Aerial Manipulator Kinematics
    *
    * Compute the UAM Jacobian and the position of the quadrotor
    *
    */      
    static void UAMKine(const UAM::CQuad& quad, UAM::CArm& arm, UAM::CHT& HT, UAM::CUAM& uam);
};

} // End of UAM namespace

#endif
