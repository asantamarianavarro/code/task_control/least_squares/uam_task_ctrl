#ifndef _COMMON_FC_H
#define _COMMON_FC_H

// MISC
#include <math.h>       /* fabs */
#include <time.h>       /* time_t, struct tm, time, localtime */
#include <string>       /* string */
#include <stdio.h>      /* input, output operations */
#include <iostream>     /* input, output operations */
#include <pwd.h>        /* password */
#include <sys/stat.h>   /* file, folder, path info */
#include <unistd.h>
#include <sys/types.h>

// Eigen
#include <eigen3/Eigen/Dense>

// KDL
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>

namespace UAM{

class CCommon_Fc
{
  private:

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  	/**
    * \brief Constructor
    *
    * Class constructor.
    *
    */
    CCommon_Fc();

  	/**
    * \brief Destructor
    *
    * Class destructor.
    *
    */
    ~CCommon_Fc();

    /**
    * \brief Get weighted generalized Jacobian inverse
    *
    * Compute the Jacobian inverse weighted depending on target distance.
    *
    */
    static Eigen::MatrixXd WeightInvJac(const Eigen::MatrixXd& J, const Eigen::MatrixXd& invjac_delta_gain, const double& invjac_alpha_min, const double& target_dist);

    /**
    * \brief Matrix pseudo-inverse
    *
    * Compute the matrix pseudo-inverse using SVD
    */
    static Eigen::MatrixXd CalcPinv(const Eigen::MatrixXd& a, double epsilon = std::numeric_limits<double>::epsilon());

    /**
    * \brief KDL Frame to Homogeneous transform
    *
    * Compute the conversion from KDL Frame to Homogeneous transform (Eigen Matrix 4d)
    */
    static Eigen::Matrix4d GetTransform(const KDL::Frame &f);

    /**
    * \brief 6-Vec to Homogeneous transform
    *
    * Compute the conversion from 6-vector (doubles) to Homogeneous transform (Eigen Matrix 4d)
    */
    static Eigen::Matrix4d GetTransform(const Eigen::VectorXd& vec);

    /**
    * \brief 3x3 Wronskian
    *
    * Compute the 3x3 Wronskian (W) using roll (phi) and pitch (theta).
    * It translates angular velocities (w) in body frame to euler angle derivatives (de) in inertial frame,
    * such as:
    * de = W·w  -> w = W^-1·de
    */
    static Eigen::Matrix3d GetWronskian(const double& phi, const double& theta);

    /**
    * \brief Skew Symmetric Matrix
    *
    * Computes the Skew Symmetric Matrix from 3-vec
    */
    static Eigen::Matrix3d Skew(const Eigen::Vector3d& vec);

    /**
    * \brief Get system date time
    *
    * Obtain system date time
    */
    static std::string get_datetime();

    /**
    * \brief Write to file
    *
    * Write to file some vars to plot them externally. Input data must be an Eigen MatrixX
    *
    */
    static void write_to_file(const std::string& folder_name, const std::string& file_name, const Eigen::MatrixXd& data, const double& ts);

};

} // End of UAM namespace

#endif
