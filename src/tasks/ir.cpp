// Own
#include "ir.h"
#include <common_fc.h>

// MISC
#include <iostream>      /* input, output operations */

namespace UAM {

CTaskIR::CTaskIR(){}

CTaskIR::~CTaskIR(){}

void CTaskIR::TaskErrorJac(const int& arm_dof, const double& inf_rad, const Eigen::MatrixXd& d_obs, Eigen::MatrixXd& sigmaIR, Eigen::MatrixXd& JIR, Eigen::MatrixXd& JIR_pseudo)
{
  // Inflation radius
  Eigen::MatrixXd inf_radius = Eigen::MatrixXd::Zero(1,1);
  inf_radius(0,0) = inf_rad;

  // Task vector 
  Eigen::MatrixXd euc_d_obs = (d_obs.transpose()*d_obs).array().sqrt();
  sigmaIR = euc_d_obs-inf_radius; 

  // Task Jacobian
  JIR = Eigen::MatrixXd::Zero(1,4+arm_dof);
  Eigen::MatrixXd Jtemp = d_obs;
  JIR.block(0,0,1,3) = 2*Jtemp.transpose();

  // Bloquing matrix
  Eigen::MatrixXd H = Eigen::MatrixXd::Zero(4+arm_dof,4+arm_dof);

  // TODO: Currently checking each dimension (x,y,z) separately
  // Euclidean distance to the obstacle
  // if (sqrt(pow(d_obs(0,0),2)+pow(d_obs(1,0),2)+pow(d_obs(2,0),2))<inf_radius(0,0)) 
  // {
    // DOF to block
    for (unsigned int ii = 0; ii < 3; ++ii)
    {
      if (d_obs(ii,0) < inf_radius(0,0) && d_obs(ii,0) > 0.0)
      {
        std::cout << "WARNING!! Inflation Radius violation. DOF: " << ii << " Inf. radius: " << inf_radius(0,0) << " Obstacle distance: " << d_obs(ii,0) << std::endl;
        H(ii,ii) = 1.0;
      }
    }
  // }

  Eigen::MatrixXd Hinv = UAM::CCommon_Fc::CalcPinv(H);
  
  // weighting matrix
  Eigen::MatrixXd temp = JIR*Hinv*JIR.transpose();  
  JIR_pseudo = Hinv*JIR.transpose()*temp.transpose();

  // DEBUG:
  // std::cout << "xxxxxx Task velocities xxxxxx" << std::endl;
  // std::cout << JIR_pseudo*sigmaIR << std::endl;
  // std::cout << "______ Task Jacobian _____" << std::endl;
  // std::cout << JIR << std::endl;
  // std::cout << "_______ Pseudo Inverse of Task Jacobian ____" << std::endl;
  // std::cout << JIR_pseudo << std::endl;
  // std::cout << "+++++++++" << std::endl;
}

} // End of UAM namespace